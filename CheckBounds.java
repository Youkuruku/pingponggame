package sample;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * Created by jordan on 29/04/17.
 */
public class CheckBounds {

    private double rectHeight = 80, screenWidth = 600, screenHeight = 400,
    ballRadious = 10, rectWidth = 20;

    /*check top bounds so the playerObject doesn't move out of bounds*/
    public  boolean checkTopBounds(Rectangle rect){
        if(rect.getY() == 0){
            return false;
        }
        return true;
    }

        /*check bot bounds so the playerObject doesn't move out of bounds*/
        public  boolean checkBotBounds(Rectangle rect){
            if(rect.getY() == screenHeight - rectHeight){
                return false;
            }
            return true;
        }

        public boolean checkBallWallBounds(Circle ball){
            if(ball.getCenterY() == (screenHeight - ballRadious) || ball.getCenterY() == ballRadious){
                return true;
            }
            return false;
        }

        public boolean checkBallRectangularCollision(Circle ball, Rectangle rect1, Rectangle rect2){
            if(ball.getCenterX() == (rectWidth + ballRadious) ||
                    ball.getCenterX() == (screenWidth -(rectWidth + ballRadious))){
                if(ball.getCenterY() >= rect1.getY() && ball.getCenterY() <= rect1.getY() + rectHeight ){
                    return true;
                }
                if(ball.getCenterY() >= rect2.getY() && ball.getCenterY() <= rect2.getY() + rectHeight ){
                    return true;
                }
            }
            return false;
        }

        public boolean checkPlayer1Bounds(Circle ball){
            if(ball.getCenterX() == 0){

                return true;
            }
            return false;
        }

    public boolean checkPlayer2Bounds(Circle ball){
        if(ball.getCenterX() == screenWidth){
            return true;
        }
        return false;
    }



}
