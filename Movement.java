package sample;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * Created by jordan on 29/04/17.
 */
public class Movement {

    private static double speed = 10;

    /*move players objects up*/
     public static void moveUp(Rectangle rect){
         rect.setY(rect.getY() - speed );
    }

    /*    /*move players objects down*/
    public static void moveDown(Rectangle rect){
        rect.setY(rect.getY() + speed );
    }


    public static void moveBall(Circle ball, double Xvel, double Yvel){
         ball.setCenterY(ball.getCenterY() + Yvel);
        ball.setCenterX(ball.getCenterX() + Xvel);
    }

}
