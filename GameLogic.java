package sample;

;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jordan on 30/04/17.
 */
public class GameLogic {

    private CheckBounds checkBounds = new CheckBounds();

    Controller c;
    double Xvel = -10, Yvel = 10, screenHeight = 400, screenWidth = 600;
    private static int player1Counter = 0, player2Counter = 0;

    public GameLogic(Controller c) {
        this.c = c;
    }


    public void beginGame() {

        Timer timer = new Timer();
        c.getButton2().setText("Play game again");
        //keep score when game restarts
        c.getPlayer2Label().setText("player2 score:" + player2Counter);
        c.getPlayer1Label().setText("player1 score:" + player1Counter);

        //move ball and restart game
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ballMovement();
                checkCollisions();
                playersScore();
            }
        }, 0, 33);
    }



    private void ballMovement() {
        Movement.moveBall(c.getBall(), Xvel, Yvel);
    }

    private void checkCollisions() {

        if (checkBounds.checkBallWallBounds(c.getBall())) {
            Yvel = -Yvel;
        }
        if (checkBounds.checkBallRectangularCollision(c.getBall(), c.getRect1(), c.getRect2())) {
            Xvel = -Xvel;
        }
    }

    private void playersScore() {
        if (checkBounds.checkPlayer1Bounds(c.getBall())) {
            player2Counter++;
            System.out.println("player2 score:" + player2Counter);
            c.explosionAnimation1();
            restartGame();
        }
        if (checkBounds.checkPlayer2Bounds(c.getBall())) {
            player1Counter++;
            System.out.println("player1 score:" + player1Counter);
            c.explosionAnimation2();
            restartGame();
        }

    }


    private void restartGame() {
        c.getBall().setCenterY(screenHeight / 2);//place ball in the middle again
        c.getBall().setCenterX(screenWidth / 2);
        Xvel = 0;
        Yvel = 0;
        c.getButton2().setVisible(true);
    }


}

