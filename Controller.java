package sample;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;


public class Controller {

    private KeyInput keyInput = new KeyInput();
    private String name = "Start 2";

    @FXML
    private Rectangle rect1;

    @FXML
    private Rectangle rect2;

    @FXML
    private AnchorPane pane1;

    @FXML
    private Button button2;

    @FXML
    private Circle ball;

    @FXML
    private Label player1Label;

    @FXML
    private Label player2Label;

    @FXML
    private ImageView player1Animation, player2Animation;


    @FXML
    public  void initialize(){
        player1Label.setText("Player 1 score: " );
        player2Label.setText("Player 2 score:");
        pane1.requestFocus();
    }



    @FXML
    void buttonMethod(ActionEvent event){
        Button b = (Button) event.getSource();
        if(b.equals(button2)){
            GameLogic g = new GameLogic(this);
            g.beginGame();
            button2.setVisible(false);
            //this line is needed so that the keyHandlers may work
            pane1.requestFocus();

        }
    }

    @FXML
    private void handleKeyInput(KeyEvent key){
        keyInput.handleMovement(key, this);
    }

    @FXML
    private void handleKeyRelease(KeyEvent key){
        System.out.println("key released:"+ key.getCode());
    }


    public Rectangle getRect1() {
        return rect1;
    }



    public AnchorPane getPane1() {
        return pane1;
    }

    public Rectangle getRect2() {
        return rect2;
    }

    public Circle getBall() {
        return ball;
    }

    public void setBall(Circle ball) {
        this.ball = ball;
    }

    public Button getButton2() {
        return button2;
    }

    public Label getPlayer1Label() {
        return player1Label;
    }

    public Label getPlayer2Label() {
        return player2Label;
    }

    public void explosionAnimation1(){
        Node explosion = player1Animation;
        explosion.setVisible(true);
        FadeTransition ft = new FadeTransition(Duration.millis(1000), explosion);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

    public void explosionAnimation2(){
        Node explosion = player2Animation;
        explosion.setVisible(true);
        FadeTransition ft = new FadeTransition(Duration.millis(1000), explosion);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ft.play();
    }

}
