package sample;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Created by jordan on 29/04/17.
 */
public class KeyInput {

    private CheckBounds checkBounds = new CheckBounds();

    protected void handleMovement(KeyEvent key, Controller c){
        System.out.println("Key Pressed: " + key.getCode());

        /*move left playerObject up*/
        if(key.getCode() == KeyCode.W){
            if(checkBounds.checkTopBounds(c.getRect1())){
                Movement.moveUp(c.getRect1());
            }
        }
        /*move right playerObject up*/
        if(key.getCode() == KeyCode.UP){
            if(checkBounds.checkTopBounds(c.getRect2())){
            Movement.moveUp(c.getRect2());
            }
        }
        /*move left playerObject down*/
        if(key.getCode() == KeyCode.S){
            if(checkBounds.checkBotBounds(c.getRect1())){
                Movement.moveDown(c.getRect1());
            }
        }
        /*move right playerObject down*/
        if(key.getCode() == KeyCode.DOWN){
            if(checkBounds.checkBotBounds(c.getRect2())){
                Movement.moveDown(c.getRect2());
            }
        }


        c.getPane1().requestFocus();
    }


}
